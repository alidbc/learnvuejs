new Vue({
	el: '#post',


	data: {

		isVisible: true,
		liked: false,
		likedCount: 10
	},


	methods: {
		toggleLike: function(){
			this.liked = !this.liked;
			this.liked ? this.likedCount++ : this.likedCount--;
		}
	}
});

new Vue({
	el: '#demo',

	data: {

		sortKey: '',
		reverse: false,
		search: '',
		columns: ['name', 'age'],
		people: [

		{name: 'Hussain', age: 20},
		{name: 'Dbc', age: 25},
		{name: 'Salam', age: 15},
		{name: 'Alwan', age: 2},
		{name: 'Hassan', age: 5},
		{name: 'Aya', age: 12},
		{name: 'Alaa', age: 37},
		{name: 'Ashour', age: 30},
		{name: 'Dubaisi', age: 44}

		]
	},

	methods: {
		sortBy: function(sortKey){
			this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
			this.sortKey = sortKey;
		}
	}
})